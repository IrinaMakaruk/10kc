# Welcome to the 10KC Project!

This project is structured into two main parts: the server (backend) and the client (frontend). Below you will find the instructions to set up and run both parts locally on your machine.

## Getting Started

### Prerequisites

Ensure you have the following installed:
- Node.js
- npm (Node Package Manager)
- MongoDB
- Git

### Clone the Repository

To get started with the 10KC project, clone the repository to your local machine:
```bash
git clone https://gitlab.com/IrinaMakaruk/10kc.git
cd 10kc
```
### Starting the Server

1.Navigate to the server directory:
```
cd server
```
2. Install the necessary npm packages:
```
npm install
```

3.Start the server using:
```
npm start
```
This will start the backend server typically on http://localhost:3030.

### Starting the Client

1. Open a new terminal and navigate to the client directory:
```
cd ../client
```
2. Install the necessary npm packages:
```
npm install
```
3. Start the client using:
```
ng serve
```
This will start the frontend client on http://localhost:4200.

### Configuring MongoDB
Ensure that you have MongoDB running on your local machine or have access to a MongoDB instance. Application by default uses my hosted instance connection string to connect to MongoDB.

You can use your own instance, for this replace at the string bellow `yourUsername`, `yourPassword`, `yourMongoDBClusterURL`, and `myDatabase` with your actual MongoDB details to use your own instance.


```
mongodb+srv://yourUsername:yourPassword@yourMongoDBClusterURL/myDatabase?retryWrites=true&w=majority
```
Go to

```
cd server/db.ts
```

And replace with your DB string


Contact Me
Iryna Makaruk irunamakaruk@gmail.com

Project Link: [10KC project](https://gitlab.com/IrinaMakaruk/10kc)
