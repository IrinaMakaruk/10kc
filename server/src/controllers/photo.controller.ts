import { Request, Response } from 'express';
import Photo from '../models/photo.model';
import fs from 'fs-extra';
import path from 'path';

export async function getPhotos(
  req: Request,
  res: Response
): Promise<Response> {
  try {
    const userId = req.query.userId as string;
    if (!userId) {
      return res.status(400).send({ message: 'UserId is required' });
    }
    const photos = await Photo.find({ userId: userId }).lean();
    return res.json(photos);
  } catch (err: any) {
    return res.status(500).send({ message: err.message });
  }
}

export async function createPhoto(
  req: Request,
  res: Response
): Promise<Response> {
  try {
    if (!req.file) {
      return res.status(400).send({ message: 'File is required' });
    }
    const newPhoto = new Photo({
      filename: req.file.filename,
      path: req.file.path,
      userId: req.body.userId,
    });
    await newPhoto.save();
    return res.json({
      message: 'Photo successfully saved',
      photo: newPhoto,
    });
  } catch (err: any) {
    return res.status(500).send({ message: err.message });
  }
}

export async function deletePhoto(
  req: Request,
  res: Response
): Promise<Response> {
  try {
    const photo = await Photo.findByIdAndDelete(req.params.id).lean();
    if (!photo) {
      return res.status(404).send({ message: 'Photo not found' });
    }
    await fs.unlink(path.resolve(photo.path));
    return res.json({ message: 'Photo Deleted' });
  } catch (err: any) {
    return res.status(500).send({ message: err.message });
  }
}
