import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import User  from '../models/user.model';
import { authConfig } from '../config/auth.config';

export const signup = async (req: Request, res: Response): Promise<void> => {
  try {
    const user = new User({
      username: req.body.username,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 8),
    });

    await user.save();
    res.send({ message: 'User was registered successfully!' });
  } catch (err) {
    if (err instanceof Error) {
      res.status(500).send({ message: err.message });
    }
  }
};

export const signin = async (req: Request, res: Response): Promise<void> => {
  try {
    const user = await User.findOne({username: req.body.username }).exec();

    if (!user) {
      res.status(404).json({
        message: 'User not found',
        status: '404 Not Found',
      });
      return;
    }

    const passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
    if (!passwordIsValid) {
      res.status(400).json({
        message: 'Invalid password',
        status: '400 Bad Request',
      });
      return;
    }

    const accessToken = jwt.sign({ id: user._id }, authConfig.secret, {
      algorithm: 'HS256',
      expiresIn: 86400 // 24 hours
    });

    res.status(200).send({
      id: user._id,
      username: user.username,
      email: user.email,
      accessToken
    });
  } catch (err) {
    if (err instanceof Error) {
      res.status(500).send({ message: err.message });
    }
  }
};

export const signout = async (req: Request, res: Response): Promise<void> => {
  try {
    res.status(200).send({ message: 'You`ve been signed out!' });
  } catch (err) {
    if (err instanceof Error) {
      res.status(500).send({ message: err.message });
    }
  }
};
