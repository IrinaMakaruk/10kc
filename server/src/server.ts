import express from 'express';
import session from 'express-session';
import cors from 'cors';
import path from 'path';
import fs from 'fs';

import { connect } from './db';
import setupAuthRoutes from './routes/auth.routes';
import setupUserRoutes from './routes/user.routes';
import setupPhotoRoutes from './routes/photo.routes';
import { authConfig } from './config/auth.config';

const app = express();

if(!fs.existsSync('uploads')) fs.mkdirSync('uploads', { recursive: true });

app.use(cors({
  credentials: true,
  origin: ['http://localhost:4200'],
  methods: ['GET', 'POST', 'DELETE'],
  allowedHeaders: 'Content-Type, Authorization'
}));

app.use(express.json());

app.use(session({
  secret: authConfig.secret,
  resave: false,
  saveUninitialized: true,
  cookie: { secure: 'auto', httpOnly: true }
}));

(async () => {
  try {
    await connect();
    app.listen(process.env.PORT || 3030);
    console.log(`Server running on port ${process.env.PORT || 3030}`);
  } catch (e) {
    console.error(e);
  }
})();

app.use('/uploads', express.static(path.resolve('uploads')));

setupAuthRoutes(app);
setupUserRoutes(app);
setupPhotoRoutes(app);
