import multer from 'multer';
import { v4 as uuidv4 } from 'uuid';
import path from 'path';

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, 'uploads/');
	},
	filename: (req, file, cb) => {
		const fileExtension = path.extname(file.originalname);
		const newFilename = `${uuidv4()}${fileExtension}`;
		cb(null, newFilename);
	}
});

const upload = multer({ storage });

export default upload;
