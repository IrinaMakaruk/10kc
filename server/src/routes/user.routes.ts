import { Express } from 'express';
import { Router } from 'express';
import { authJwt } from '../middlewares/authJwt';

const setupUserRoutes = (app: Express) => {
  const router = Router();
  router.get('/api/user', authJwt);
  app.use(router);
};

export default setupUserRoutes;
