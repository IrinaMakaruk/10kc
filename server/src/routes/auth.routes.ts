import { Express } from 'express';
import { signup, signin, signout } from '../controllers/auth.controller';
import { checkDuplicateUsernameOrEmail } from '../middlewares/verifySignUp';

const setupAuthRoutes = (app: Express) => {
  app.post('/api/auth/signup', checkDuplicateUsernameOrEmail, signup);
  app.post('/api/auth/signin', signin);
  app.post('/api/auth/signout', signout);
};

export default setupAuthRoutes;
