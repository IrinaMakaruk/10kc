import { Express } from 'express';
import multer from '../multer';
import {
  createPhoto,
  getPhotos,
  deletePhoto,
} from '../controllers/photo.controller';
import authJwt from '../middlewares/authJwt';

const setupPhotoRoutes = (app: Express) => {
  app
    .route('/api/photos')
    .get(getPhotos, authJwt)
    .post(multer.single('image'), createPhoto, authJwt);

  app.route('/api/photos/:id').delete(deletePhoto, authJwt);
};

export default setupPhotoRoutes;
