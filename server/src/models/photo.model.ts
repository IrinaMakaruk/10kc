import mongoose, { Schema, Document } from 'mongoose';

interface IPhoto extends Document {
  filename: string;
  path: string;
  uploadedDate: Date;
  userId: Object;
}

const photoSchema = new Schema<IPhoto>({
  filename: { type: String, required: true },
  path: { type: String, required: true },
  uploadedDate: { type: Date, required: true, default: Date.now },
  userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true }
});

const Photo = mongoose.model<IPhoto>('Photo', photoSchema);
export default Photo;
