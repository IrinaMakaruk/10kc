import jwt from 'jsonwebtoken';
import { Request, Response, NextFunction } from 'express';
import { authConfig } from '../config/auth.config';

interface RequestWithUserId extends Request {
  userId?: string;
}

export const authJwt = (req: RequestWithUserId, res: Response, next: NextFunction) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];

  if (!token) {
    return res.status(403).send({ message: 'No token provided!' });
  }

  jwt.verify(token, authConfig.secret, (err: any, decoded: any) => {
    if (err) {
      return res.status(401).send({ message: 'Unauthorized!' });
    }
    req.userId = decoded.id;
    next();
  });
};

export default authJwt;
