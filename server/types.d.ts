import 'express';
import { File } from 'multer';
import session from 'express-session';

declare module 'express-session' {
  interface SessionData {
    accessToken?: string;
  }
}

declare module 'express' {
  interface Request {
    file?: File;
  }
}
