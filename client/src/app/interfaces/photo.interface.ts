export interface Photo {
  filename: string;
  path: string;
  uploadedDate: string;
  userId: string;
}