import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from './services/auth.service';
import { StorageService } from './services/storage.service';
import { User } from './interfaces/user.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  isLoggedIn = false;
  username?: string;
  user!: User;

  private authSubscription!: Subscription;

  constructor(
    private storageService: StorageService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.authSubscription = this.storageService
      .getLoggedInStatus()
      .subscribe((status) => {
        if (status) {
          this.user = this.storageService.getUser()!;
          this.isLoggedIn = true;
          this.username = this.user?.username;
        } else {
          this.isLoggedIn = false;
          this.username = undefined;
        }
      });
  }

  logout(): void {
    this.authService.logout().subscribe({
      next: () => {
        this.storageService.clean();
        window.location.reload();
      },
      error: (err) => console.error('Logout Error:', err),
    });
  }

  ngOnDestroy() {
    this.authSubscription.unsubscribe();
  }
}
