import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';

import { User } from '../interfaces/user.interface';
import { USER_KEY } from '../helpers/config';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  private loggedInStatus: BehaviorSubject<boolean> =
    new BehaviorSubject<boolean>(false);

  constructor() {
    this.checkInitialLoginStatus();
  }

  clean(): void {
    window.sessionStorage.clear();
    this.emitLoggedInStatus(false);
  }

  saveUser(user: User): void {
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
    this.emitLoggedInStatus(true);
  }

  getUser(): User | null {
    const user = window.sessionStorage.getItem(USER_KEY);
    return user ? JSON.parse(user) : null;
  }

  isLoggedIn(): boolean {
    return this.loggedInStatus.value;
  }

  getLoggedInStatus(): Observable<boolean> {
    return this.loggedInStatus.asObservable();
  }

  private checkInitialLoginStatus(): void {
    const user = window.sessionStorage.getItem(USER_KEY);
    this.emitLoggedInStatus(!!user);
  }

  private emitLoggedInStatus(status: boolean): void {
    this.loggedInStatus.next(status);
  }
}
