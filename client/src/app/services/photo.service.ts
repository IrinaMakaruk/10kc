import { StorageService } from './storage.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, catchError, map, throwError } from 'rxjs';

import { Photo } from '../interfaces/photo.interface';
import { API_URL } from '../helpers/config';

@Injectable({
  providedIn: 'root',
})
export class PhotosService {
  constructor(
    private storageService: StorageService,
    private http: HttpClient
  ) {}

  loadPhotos(): Observable<Photo[]> {
    const user = this.storageService.getUser();
    if (!user) {
      return throwError(() => new Error('User not found'));
    }

    return this.http
      .get<Photo[]>(`${API_URL}api/photos`, { params: { userId: user.id! } })
      .pipe(
        map((photos) =>
          photos.map((photo) => ({
            ...photo,
            path: `${API_URL}uploads/${photo.filename}`,
          }))
        ),
        catchError((error) =>
          throwError(() => new Error(`Failed to load photos: ${error}`))
        )
      );
  }

  uploadPhoto(formData: FormData): Observable<any> {
    return this.http
      .post(API_URL + 'api/photos', formData)
      .pipe(catchError(this.handleError));
  }

  deletePhoto(id: string): Observable<any> {
    return this.http
      .delete(`${API_URL}api/photos/${id}`)
      .pipe(catchError(this.handleError));
  }

  mapPhotoFileForView(photo: Photo): Photo {
    return {
      ...photo,
      path: `${API_URL}uploads/${photo.filename}`,
    };
  }

  private handleError(error: any) {
    console.error('An error occurred:', error.error.message);
    return throwError(
      () => new Error('Something bad happened; please try again later.')
    );
  }
}
