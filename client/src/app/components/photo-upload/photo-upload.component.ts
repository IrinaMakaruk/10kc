import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

import { PhotosService } from '../../services/photo.service';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-photo-upload',
  templateUrl: './photo-upload.component.html',
  styleUrls: ['./photo-upload.component.scss'],
})
export class PhotoUploadComponent {
  selectedFile: File | null = null;
  fileError: string | null = null;

  constructor(
    private storageService: StorageService,
    private photosService: PhotosService,
    public dialogRef: MatDialogRef<PhotoUploadComponent>
  ) {}

  onSubmit(): void {
    if (!this.selectedFile) {
      return;
    }
    const user = this.storageService.getUser();

    const formData = new FormData();
    formData.append('image', this.selectedFile, this.selectedFile.name);
    formData.append('userId', user!.id!);
    this.photosService.uploadPhoto(formData).subscribe({
      next: ({ photo }) => {
        const photoForView = this.photosService.mapPhotoFileForView(photo);
        this.dialogRef.close(photoForView);
      },
      error: (error) => {
        console.error('Failed to upload photo', error);
      },
    });
  }

  onFileSelected(event: Event): void {
    const target = event.target as HTMLInputElement;
    const file: File = (target.files as FileList)[0];
    this.selectedFile = file;
  }
}
