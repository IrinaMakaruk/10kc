import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { of } from 'rxjs';

import { HttpClientTestingModule } from '@angular/common/http/testing';

import { PhotoUploadComponent } from './photo-upload.component';
import { PhotosService } from '../../services/photo.service';
import { StorageService } from '../../services/storage.service';
import { User } from '../../interfaces/user.interface';

describe('PhotoUploadComponent', () => {
  let component: PhotoUploadComponent;
  let fixture: ComponentFixture<PhotoUploadComponent>;
  let mockPhotosService: jasmine.SpyObj<PhotosService>;
  let mockStorageService: jasmine.SpyObj<StorageService>;
  let mockDialogRef: jasmine.SpyObj<MatDialogRef<PhotoUploadComponent>>;
  const mockedUser: User = {
    id: '123',
    username: 'Test User',
    email: 'test@email.com',
    password: 'testpass',
    accessToken: 'myToken'
  };

  beforeEach(async () => {
    mockPhotosService = jasmine.createSpyObj('PhotosService', [
      'uploadPhoto',
      'mapPhotoFileForView',
    ]);
    mockStorageService = jasmine.createSpyObj('StorageService', ['getUser']);
    mockDialogRef = jasmine.createSpyObj('MatDialogRef', ['close']);

    await TestBed.configureTestingModule({
      declarations: [PhotoUploadComponent],
      imports: [
        MatIconModule,
        MatInputModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
      ],
      providers: [
        { provide: PhotosService, useValue: mockPhotosService },
        { provide: StorageService, useValue: mockStorageService },
        { provide: MatDialogRef, useValue: mockDialogRef },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(PhotoUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should handle file selection', () => {
    const inputElement = document.createElement('input');
    inputElement.type = 'file';
    const event = { target: inputElement } as unknown as Event;
    component.onFileSelected(event);

    expect(component.selectedFile).toBeUndefined();
  });

  it('should not submit if no file is selected', () => {
    component.onSubmit();
    expect(mockPhotosService.uploadPhoto).not.toHaveBeenCalled();
  });

  it('should submit the selected file', () => {
    const testFile = new File([''], 'test.jpg');
    component.selectedFile = testFile;
    mockStorageService.getUser.and.returnValue(mockedUser);
    mockPhotosService.uploadPhoto.and.returnValue(
      of({ photo: { id: '123', path: 'path/to/photo' } })
    );

    component.onSubmit();

    expect(mockPhotosService.uploadPhoto).toHaveBeenCalled();
  });
});
