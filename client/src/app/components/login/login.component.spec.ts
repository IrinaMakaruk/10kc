import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { Router } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';

import { StorageService } from '../../services/storage.service';
import { AuthService } from '../../services/auth.service';
import { LoginComponent } from './login.component';

class MockAuthService {
  login(username: string, password: string) {
    return {
      subscribe: (handlers: any) =>
        handlers.next({ username, token: 'fake-token' }),
    };
  }
}

class MockStorageService {
  saveUser(user: any) {}
  isLoggedIn() {
    return false;
  }
}

class MockRouter {
  navigate(url: string[]) {}
}

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let storageService: StorageService;
  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [
        BrowserAnimationsModule,
        MatCardModule,
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
      ],
      providers: [
        { provide: AuthService, useClass: MockAuthService },
        { provide: StorageService, useClass: MockStorageService },
        { provide: Router, useClass: MockRouter },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    storageService = TestBed.inject(StorageService);
    router = TestBed.inject(Router);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should log in successfully', () => {
    const spy = spyOn(storageService, 'saveUser').and.callThrough();
    const navigateSpy = spyOn(router, 'navigate');

    component.form.username = 'test';
    component.form.password = 'password';
    component.onSubmit();

    expect(component.isLoggedIn).toBeTrue();
    expect(component.isLoginFailed).toBeFalse();
    expect(navigateSpy).toHaveBeenCalledWith(['/profile']);
    expect(spy).toHaveBeenCalled();
  });
});
