import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';

import { PhotoGalleryComponent } from './photo-gallery.component';
import { PhotosService } from '../../services/photo.service';

import { of } from 'rxjs';

class MockPhotosService {
  loadPhotos() {
    return of([
      { _id: '1', url: 'url1' },
      { _id: '2', url: 'url2' },
    ]);
  }
  deletePhoto(id: string) {
    return of({});
  }
}

class MockMatDialog {
  open() {
    return {
      afterClosed: () => of({ _id: '3', url: 'url3' }),
    };
  }
}

describe('PhotoGalleryComponent', () => {
  let component: PhotoGalleryComponent;
  let fixture: ComponentFixture<PhotoGalleryComponent>;
  let photosService: PhotosService;
  let dialog: MatDialog;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PhotoGalleryComponent],
      providers: [
        { provide: PhotosService, useClass: MockPhotosService },
        { provide: MatDialog, useClass: MockMatDialog },
      ],
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        MatIconModule,
        MatCardModule,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(PhotoGalleryComponent);
    component = fixture.componentInstance;
    photosService = TestBed.inject(PhotosService);
    dialog = TestBed.inject(MatDialog);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load photos on init', () => {
    spyOn(photosService, 'loadPhotos').and.callThrough();
    component.ngOnInit();
    expect(photosService.loadPhotos).toHaveBeenCalled();
    expect(component.photos.length).toBe(2);
  });

  it('should open the upload dialog and add new photo on close', () => {
    const spyDialog = spyOn(dialog, 'open').and.callThrough();
    component.openUploadDialog();
    expect(spyDialog).toHaveBeenCalled();
    expect(component.photos.length).toBe(3);
  });

  it('should delete a photo', () => {
    const spyDelete = spyOn(photosService, 'deletePhoto').and.callThrough();
    component.deletePhoto('1');
    expect(spyDelete).toHaveBeenCalledWith('1');
    expect(component.photos.length).toBe(1);
    expect(component.photos[0]._id).toBe('2');
  });
});
