import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { PhotoUploadComponent } from '../photo-upload/photo-upload.component';
import { PhotosService } from '../../services/photo.service';
import { Photo } from '../../interfaces/photo.interface';

@Component({
  selector: 'app-photo-gallery',
  templateUrl: './photo-gallery.component.html',
  styleUrls: ['./photo-gallery.component.scss'],
})
export class PhotoGalleryComponent implements OnInit {
  photos: any[] = [];

  constructor(
    private photosService: PhotosService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.loadPhotos();
  }

  openUploadDialog(): void {
    const dialogRef = this.dialog.open(PhotoUploadComponent, {
      width: '300px',
    });
    dialogRef.afterClosed().subscribe((photo: Photo) => {
      if (photo) this.photos = [...this.photos, photo];
    });
  }

  deletePhoto(id: string): void {
    this.photosService.deletePhoto(id).subscribe({
      next: (_) => {
        this.photos = this.photos.filter((photo) => photo._id !== id);
      },
      error: (error) => {
        console.error('Failed to load photos', error);
      },
    });
  }

  loadPhotos() {
    this.photosService.loadPhotos().subscribe({
      next: (photos) => {
        this.photos = photos;
      },
      error: (error) => {
        console.error('Failed to load photos', error);
      },
    });
  }
}
