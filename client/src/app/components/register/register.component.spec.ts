import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';

import { RegisterComponent } from './register.component';
import { AuthService } from '../../services/auth.service';

class MockAuthService {
  register(username: string, email: string, password: string) {
    return {
      subscribe: (handlers: any) => {
        if (username !== 'fail') {
          handlers.next({ message: 'User registered successfully' });
        } else {
          handlers.error({ error: { message: 'Registration failed' } });
        }
      },
    };
  }
}

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let authService: AuthService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      imports: [
        BrowserAnimationsModule,
        MatCardModule,
        FormsModule,
        MatFormFieldModule,
        MatInputModule,
      ],
      providers: [{ provide: AuthService, useClass: MockAuthService }],
    }).compileComponents();

    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    authService = TestBed.inject(AuthService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should register successfully', () => {
    spyOn(authService, 'register').and.callThrough();
    component.form.username = 'user';
    component.form.email = 'user@example.com';
    component.form.password = 'password123';
    component.onSubmit();

    expect(authService.register).toHaveBeenCalledWith(
      'user',
      'user@example.com',
      'password123'
    );
    expect(component.isSignUpFailed).toBeFalse();
    expect(component.errorMessage).toEqual('');
  });

  it('should handle registration failure', () => {
    spyOn(authService, 'register').and.callThrough();
    component.form.username = 'fail';
    component.onSubmit();

    expect(authService.register).toHaveBeenCalled();
    expect(component.isSignUpFailed).toBeTrue();
    expect(component.errorMessage).toEqual('Registration failed');
  });
});
